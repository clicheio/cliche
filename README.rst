Cliche
======

.. image:: https://drone.io/bitbucket.org/clicheio/cliche/status.png
   :alt: Build Status
   :target: https://drone.io/bitbucket.org/clicheio/cliche/latest

Cliche is an ontology of fictional works.
